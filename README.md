# RMS HELPDESK

## Pasos a seguir

Crear una carpeta django-helpdesk

Dentro hacer un git clone del repositorio

Create virual env:

    - python3 -m venv env

Activate virtual env:

    - source env/bin/activate

Accedemos a la carpeta backend del repositorio

    - cd backend/

Instalamos los paquetes necesarios

    - pip install -r requirements.txt

Comprobar estado de las migraciones de la base de datos

    - python manage.py makemigrations
    - python manage.py migrate

Ejecutar Servidor en local

    - python manage.py runserver

Rutas

    - /admin/
    - /helpdesk/

#### Documentación Oficial del Helpdesk

(https://django-helpdesk.readthedocs.io/en/0.2.x/install.html)
